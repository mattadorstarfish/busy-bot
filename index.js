#!/usr/bin/env node

var Program = require('./bin/program.js');
const commandLineArgs = require('command-line-args');

var commander = require('commander');

commander
  .arguments('<portName>')
  .option('-k, --key <key>', 'The Slack API key to use')
  .option('-p, --port <port>', 'The port number to use')
  .action(run)
  .parse(process.argv);

  function run(portName)
  {
    var program = new Program({
        portName:portName,
        key:commander.key,
        port:commander.port
    });
    program.run();
  }

