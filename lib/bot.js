'use strict';

var util = require('util');
var path = require('path');
var fs = require('fs');
var Bot = require('slackbots');
var dbGenerator = require('./databaseGenerator.js');
var eventEmitter = require('../bin/eventEmitter.js');
var match = require('./match.js');

var BusyBot = function Constructor(settings) {
    this.settings = settings;
    this.settings.name = this.settings.name || 'areyoubusy';
    this.dbPath = settings.dbPath || path.resolve(process.cwd(), 'data', 'busybot.db');

    this.user = null;
    this.db = null;
};

BusyBot.prototype.run = function () {
    BusyBot.super_.call(this, this.settings);

    this.on('start', this._onStart);
    this.on('message', this._onMessage);
};

BusyBot.prototype._onStart = function () {
    this._loadBotUser();
    this._connectDb();
    this._firstRunCheck();
};

BusyBot.prototype._loadBotUser = function () {
    var self = this;
    this.user = this.users.filter(function (user) {
        return user.name === self.name;
    })[0];
};

BusyBot.prototype._connectDb = function () {
    if (!fs.existsSync(this.dbPath)) {
        console.error('Database path ' + '"' + this.dbPath + '" does not exists or it\'s not readable.');
        process.exit(1);
    }

    this.db = dbGenerator(this.dbPath);
};

BusyBot.prototype._firstRunCheck = function () {
    var self = this;
    
};

BusyBot.prototype._welcomeMessage = function () {
    this.postMessageToChannel(this.channels[0].name, 'Hi guys.' +
        '\n If you need me can, just say `' + this.name + '` to call me!',
        {as_user: true});
};

BusyBot.prototype._onMessage = function (message) {
    if (this._isChatMessage(message) &&
        this._isChannelConversation(message) &&
        !this._isFromBusyBot(message) &&
        this._isMentioningName(message)
    ) {
        this._replyWithStatus(message);
    }
};

BusyBot.prototype._isChatMessage = function (message) {
    return message.type === 'message' && Boolean(message.text);
};

BusyBot.prototype._isChannelConversation = function (message) {
    return typeof message.channel === 'string' &&
        message.channel[0] === 'C';
};

BusyBot.prototype._isFromBusyBot = function (message) {
    return message.user === this.user.id;
};

BusyBot.prototype._isMentioningName = function (message) {
    return message.text.toLowerCase().indexOf(this.name) > -1 
        || message.text.indexOf(this.user.id) > -1 ;
};

BusyBot.prototype._getChannelById = function (channelId) {
    return this.channels.filter(function (item) {
        return item.id === channelId;
    })[0];
};

BusyBot.prototype._replyWithStatus = function (originalMessage) {
    var self = this;
    console.log("Got message");
    var channel = self._getChannelById(originalMessage.channel);
    var user = self._getUserFromMessage(originalMessage.text);
    var status = self._getUserPresence(user);
    self._notifyArduino(status);
    var textStatus = status === 'true' ? "available" : "busy";
    console.log(textStatus);
    self.postMessageToChannel(channel.name, "<@" + user.name + "> is " + textStatus + ".", {as_user: true});
};

BusyBot.prototype._getUserFromMessage = function(message)
{
    var self = this;
    var data = match(message, '<@...>');
    var user = self._getUser(data[1]);
    return user;
}

BusyBot.prototype._getUserPresence = function(user)
{
    var presence = user.presence;
    console.log(presence);
    var status = presence.toLowerCase() === 'active' ? 'true' : 'false';
    return status;
}

BusyBot.prototype._getUser = function(id)
{
    this._updateUsers();
    return this.users.filter(function (user) {
        return user.id === id;
    })[0];
}

BusyBot.prototype._updateUsers = function()
{
    var self = this;
    return this.getUsers().then(function(users){
        self.users == users;
    });
}

BusyBot.prototype._notifyArduino = function(status)
{
    console.log("Notify");
    console.log(eventEmitter.emit("botReply", status));
}

// inherits methods and properties from the Bot constructor
util.inherits(BusyBot, Bot);

module.exports = BusyBot;

