'use strict';

var path = require('path');
var sqlite3 = require('sqlite3').verbose();

function generate(dbPath)
{
    var outputFile = dbPath || path.resolve(__dirname, 'busybot.db');
    var db = new sqlite3.Database(outputFile);
    // Prepares the database connection in serialized mode
    db.serialize();
    // Creates the database structure
    db.run('CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY, username TEXT, busy INTEGER DEFAULT 0)');

    return db;
}

 module.exports = generate;