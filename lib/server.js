var WebSocketServer = require('ws').Server;

module.exports = Server;

function Server(port, portNumber)
{
    var self = this;
    this.port = port;  
    this.portNumber = portNumber;           // port number for the webSocket server
    this.wss = new WebSocketServer({port: this.portNumber}); // the webSocket server
    this.connections = new Array;          // list of connections to the server
 
    this.handleConnection = function(client) {
        console.log("New Connection"); // you have a new client
        self.connections.push(client); // add this client to the connections array

        client.on('message', self.sendToSerial); // when a client sends a message,

        client.on('close', function() { // when a client closes its connection
            console.log("connection closed"); // print it out
            var position = self.connections.indexOf(client); // get the client's position in the array
            self.connections.splice(position, 1); // and delete it from the array
        });
    };

    this.sendToSerial = function(data) {
        console.log("sending to serial: " + data);
        self.port.write(data);
    };

    // This function broadcasts messages to all webSocket clients
    this.broadcast = function (data) {
        for (myConnection in self.connections) {   // iterate over the array of connections
            self.connections[myConnection].send(data); // send the data to each connection
        }
    }

    this.saveLatestData = function (data) {
        console.log(data);
        // if there are webSocket connections, send the serial data
        // to all of them:
        if (connections.length > 0) {
            broadcast(data);
        }
    };

    this.wss.on('connection', self.handleConnection);
}