var SerialPort = require('serialport');
var Server = require('../lib/server.js');
var BusyBot = require('../lib/bot.js');
var eventEmitter = require('../bin/eventEmitter.js');

function Program(args) {
    var self = this;

    this.bot = null;
    this.port = null;
    this.server = null;

    this.args = args;
    this.portName = args.portName || null;
    this.key = args.key || null;
    this.serverPort = args.port || 8081;
    
    this.serverRunning = false;

    this.run = function()
    {
        if(this.portName !== null)
        {
            this.serverRunning = this.initServer();
        }

        this.initBot();
        this.bot.run();

        eventEmitter.on('botReply', this.sendToServer);
    };

    this.sendToServer = function(status)
    {
        if(self.server !== null)
        {
            self.server.sendToSerial(status);
        }
    };

    this.initBot = function()
    {
        var dbPath = process.env.BOT_DB_PATH;
        var name = process.env.BOT_NAME;

        this.bot = new BusyBot({
            token: this.key,
            dbPath: dbPath,
            name: name
        });
    };

    this.initServer = function()
    {
        try
        {
            this.port = new SerialPort(this.portName, {
                baudRate: 9600,
            });

            this.server = new Server(this.port, this.serverPort);
        }
        catch(err)
        {
            console.log(err);
            return false;
        }
       
        return true;
    };
};

module.exports = Program;
